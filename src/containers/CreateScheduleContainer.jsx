import React from "react";
import { useStoreActions } from "easy-peasy";

import {
    Space,
    Breadcrumb,
    Divider,
    Typography,
    Button,
    Form,
    Input,
    DatePicker,
    TimePicker,
    Modal
} from "antd";

import { 
    FileOutlined,
} from "@ant-design/icons";
  

const {Title, Text} = Typography; 
const { RangePicker } = TimePicker;

const CreateScheduleContainer = () => {
    const submitSchedule = useStoreActions(actions => actions.adminCrudSchedule.submitSchedule);
    const [modal, contextHolder] = Modal.useModal();
    const [form] = Form.useForm();

    const onCreate = () => {
        form
            .validateFields()
            .then(async (value) => {
                let data = {...value, status: "Belum Dilaksanakan"};
                data.start = data.start._d;
                console.log(data);
                const res = await submitSchedule(data);
                if (res.error) {
                    modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>)})
                }
                else {
                    modal.info({title: 'Success', content:(<p>Document submitted</p>)});
                }
            })
            .catch((info) => {
                console.log(info);
                modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>)})
            });
    }

    return <div>
        <Breadcrumb style={{ margin: '3px 0' }}>
            <Breadcrumb.Item>
                <Button type="link" disabled>
                    <FileOutlined />Schedule
                </Button>
            </Breadcrumb.Item>
        </Breadcrumb>
        <Space direction="vertical" size={0} style={{ width: "100%" }}>
            <Title level={1}>Schedule Form</Title>
            <Text style={{ fontStyle: "italic"}}>Create new schedule</Text>
        </Space>
        <Divider style={{ borderWidth: 0.5, borderColor: 'black' }}></Divider>
        <Form
                form={form}
                layout="vertical"
                name="create_form"
                initialValues={{
                    modifier: "public",
                }}
            >
                <Form.Item
                    name="title"
                    label="Title"
                    rules={[
                        {
                            required: true,
                            message: "Please input the title!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item 
                    name="start" 
                    label="Date" 
                    rules={[
                        {
                            type: 'object',
                            required: true,
                            message: "Please input the date!",
                        },
                    ]}
                >
                    <DatePicker showTime format="YYYY-MM-DD " />
                </Form.Item>
                <Form.Item 
                    name="time" 
                    label="Duration" 
                    rules={[
                        {
                            type: 'object',
                            required: true,
                            message: "Please input the schedule duration!",
                        },
                    ]}
                    valuePropName="checked"
                    getValueFromEvent={e => {
                        console.log(e);
                        return {start: e[0]._d, end: e[1]._d};
                    }}
                >
                    <RangePicker  />
                </Form.Item>
                <Form.Item
                    name="location"
                    label="Location"
                    rules={[
                        {
                            required: true,
                            message: "Please input the location!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="desc"
                    label="Description"
                    rules={[
                        {
                            required: true,
                            message: "Please input the description!",
                        },
                    ]}
                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" onClick={onCreate}>
                        Create
                    </Button>
                </Form.Item>
            </Form>
            {contextHolder}
    </div>
};

export default CreateScheduleContainer;
