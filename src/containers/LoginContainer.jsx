import React from "react";
import LoginTemplate from "../components/LoginTemplate";
import { useStoreActions } from "easy-peasy";
import { useStoreState } from "easy-peasy";
import { Redirect } from "react-router-dom";

const LoginContainer = ({ from, location }) => {
  const login = useStoreActions((actions) => actions.auth.login);
  const isAuthenticated = useStoreState((state) => state.auth.isAuthenticated);
  const user = useStoreState(state => state.auth.userData);

    console.log(isAuthenticated, user);

  const onLogin = async (e,p) => {
    let res = await login(e,p);
    console.log(res);
  }

  if (isAuthenticated)
    return (
      <Redirect
        to={{
          pathname: from ? from.pathname : "/",
          state: { from: location },
        }}
      />
    );
  else
    return (
      <LoginTemplate onSubmitLogin={onLogin}></LoginTemplate>
    );
};

export default LoginContainer;
