import React, { useState, useEffect } from "react";
import FetchUserTemplate from "../components/FetchUserTemplate";
import FetchUserDetailTemplate from "../components/FetchUserDetailTemplate";
import EditUserContainer from "./EditUserContainer";
import { useStoreState, useStoreActions } from "easy-peasy";

import {
    Space,
    Breadcrumb,
    Divider,
    Typography,
    Button
} from "antd";

import { 
    TeamOutlined,
} from "@ant-design/icons";
  

const {Title, Text} = Typography; 

const FetchUserContainer = () => {
    const fetchAllUsers = useStoreActions(actions => actions.adminFetchUser.fetchAllUsers);
    const fetchUserAllAttendances = useStoreActions(actions => actions.adminFetchUser.fetchUserAllAttendances);
    const users = useStoreState(state => state.adminFetchUser.data.users);
    const attendances = useStoreState(state => state.adminFetchUser.data.attendances);

    const [modalVisible, setModalVisible] = useState(false);

    const [fetched, setFetched] = useState(false);
    if (!fetched) {
        fetchAllUsers();
        setFetched(true);
    }

    useEffect(() => {
        setContent(<FetchUserTemplate users = {users} onUserClick={onUserClick} onEdit={onUserEdit} ></FetchUserTemplate>);
    }, [users]);

    const [breadcrumb, setBreadcrumb] = useState(<Breadcrumb style={{ margin: '3px 0' }}>
            <Breadcrumb.Item>
                <Button type="link" disabled>
                    <TeamOutlined />Users
                </Button>
            </Breadcrumb.Item>
        </Breadcrumb>);

    const onUserClick = async (user) => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item onClick={onBackToAllUsersMenu}>
                    <Button type="link">
                        <TeamOutlined />Users
                    </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{user.name}</Breadcrumb.Item>
            </Breadcrumb>);
        await fetchUserAllAttendances(user.userId);
        console.log(attendances);
        setContent(<FetchUserDetailTemplate user={user} attendances={attendances}></FetchUserDetailTemplate>);
    }

    const onUserEdit = async (user) => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item onClick={onBackToAllUsersMenu}>
                    <Button type="link">
                        <TeamOutlined />Users
                    </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{user.name}</Breadcrumb.Item>
            </Breadcrumb>);
        setContent(<EditUserContainer user={user} onSubmit={onBackToAllUsersMenu}></EditUserContainer>);
    }

    const [content, setContent] = useState(<FetchUserTemplate users = {users} onUserClick={onUserClick} onEdit={onUserEdit}></FetchUserTemplate>);

    const onBackToAllUsersMenu = () => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item>
                    <Button type="link" disabled>
                        <TeamOutlined />Users
                    </Button>
                </Breadcrumb.Item>
            </Breadcrumb>);
        fetchAllUsers();
        setContent(<FetchUserTemplate users = {users} onUserClick={onUserClick} onEdit={onUserEdit} ></FetchUserTemplate>);
    };

    return <div>
        {breadcrumb}
        <Space direction="vertical" size={0} style={{ width: "100%" }}>
            <Title level={1}>User Management</Title>
            <Text style={{ fontStyle: "italic"}}>Manage all user information</Text>
        </Space>
        <Divider style={{ borderWidth: 0.5, borderColor: 'black' }}></Divider>
        {content}
    </div>
};

export default FetchUserContainer;
