import React from "react";
import { useStoreActions, useStoreState } from "easy-peasy";

import {
    Space,
    Breadcrumb,
    Divider,
    Typography,
    Button,
    Form,
    Input,
    Select,
    DatePicker,
    Upload,
} from "antd";

import { 
    TeamOutlined, MinusCircleOutlined, PlusOutlined, UploadOutlined
} from "@ant-design/icons";
import { useState } from "react";

const {Title, Text} = Typography; 
const { RangePicker } = DatePicker;

const CreateUserContainer = () => {
    const submitUser = useStoreActions(actions => actions.adminCrudUser.submitUser);
    const uploadImage = useStoreActions(actions => actions.adminCrudUser.uploadImage);
    const getImageUrl = useStoreActions(actions => actions.adminCrudUser.getImageUrl);
    const fetchAllPositions = useStoreActions(actions => actions.adminFetchPosition.fetchAllPositions);
    const positions = useStoreState(actions => actions.adminFetchPosition.data.positions);
    const [form] = Form.useForm();
    const [fetched, setFetched] = useState(false);

    if (!fetched) {
        fetchAllPositions();
        setFetched(true);
    }

    const onRegister = () => {
        form
            .validateFields()
            .then(async (value) => {
                let data = value;
                delete data.confirmPassword;
                // delete data.email;
                // delete data.password;
                data.birthdate = data.birthdate._d;
                data.year = [data.year[0]._d, data.year[1]._d];
                const url = await getImageUrl(data.photo);
                if (url.error) {
                    data.photo = undefined;
                }
                else {
                    data.photo = url.data;
                }
                console.log(data);
              submitUser(data);
            })
            .catch((info) => {
                console.log(info);
            });
    }
    const checkPhoneNumber = (rule, value) => {
        console.log('phone validator', value);
        if (value.number > 0) {
          return Promise.resolve();
        }
    
        return Promise.reject('Phone is invalid');
      };
    
      const formItemLayout = {
        labelCol: {
          xs: {
            span: 24,
          },
          sm: {
            span: 8,
          },
        },
        wrapperCol: {
          xs: {
            span: 24,
          },
          sm: {
            span: 16,
          },
        },
      };
    return <div>
        <Breadcrumb style={{ margin: '3px 0' }}>
            <Breadcrumb.Item>
                <Button type="link" disabled>
                    <TeamOutlined />Users
                </Button>
            </Breadcrumb.Item>
        </Breadcrumb>
        <Space direction="vertical" size={0} style={{ width: "100%" }}>
            <Title level={1}>User Form</Title>
            <Text style={{ fontStyle: "italic"}}>Register new user</Text>
        </Space>
        <Divider style={{ borderWidth: 0.5, borderColor: 'black' }}></Divider>
        <Form
                form={form}
                layout="vertical"
                name="create_form"
                initialValues={{
                    modifier: "public",
                }}
              {...formItemLayout}
            >
                <Form.Item
                    name="email"
                    label="Email"
                    rules={[
                        {
                            required: true,
                            message: "Please input the Email!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            message: "Please input the password!",
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    name="confirmPassword"
                    label="Confirm Password"
                    rules={[
                        {
                            required: true,
                            message: "Please input the password!",
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject('The two passwords that you entered do not match!');
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[
                        {
                            required: true,
                            message: "Please input the name!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="nik"
                    label="NIK"
                    rules={[
                        {
                            required: true,
                            message: "Please input the NIK!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="photo"
                    label="Photo"
                    rules={[
                        {
                            required: true,
                            message: "Please upload your photo!",
                        },
                    ]}
                    valuePropName="checked"
                    getValueFromEvent={e => {
                        console.log(e);
                        return e.fileList[0].name;
                    }}
                >
                    <Upload
                        customRequest={uploadImage}
                        listType='picture'
                        className='upload-list-inline'
                    >
                        <Button icon={<UploadOutlined />}>Click to Upload</Button>
                    </Upload>
                </Form.Item>
                <Form.Item
                    name="gender"
                    label="Gender"
                    rules={[
                        {
                            required: true,
                            message: "Please Select the Gender!",
                        },
                    ]}
                >
                    <Select>
                    <Select.Option value="Male">Male</Select.Option>
                    <Select.Option value="Female">Female</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="birthdate"
                    label="BirthDate"
                    rules={[
                        {
                            required: true,
                            message: "Please input your birthdate!",
                        },
                    ]}
                >
                    <DatePicker />
                </Form.Item>
                <Form.Item
                    name="education"
                    label="Education"
                    rules={[
                        {
                            required: true,
                            message: "Please input your latest education!",
                        },
                    ]}
                >
                    <Input />
                    </Form.Item>
                    <Form.Item
                    name="year"
                    label="Active Year"
                    rules={[
                        {
                            required: true,
                            message: "Please input your active year!",
                        },
                    ]}
                >
                    <RangePicker picker ='year' />
                    </Form.Item>
                <Form.Item
                    name="fraction"
                    label="Fraction"
                    rules={[
                        {
                            required: true,
                            message: "Please Select your fraction!",
                        },
                    ]}
                >
                    <Select>
                    <Select.Option value="Select">Select</Select.Option>
                    <Select.Option value="FraksiGolkar">Fraksi Golkar</Select.Option>
                    <Select.Option value="FraksiAmanatSanggam">Fraksi Amanat Sanggam</Select.Option>
                    <Select.Option value="FraksiGabunganPerubahan">Fraksi Gabungan Perubahan</Select.Option>
                    <Select.Option value="FraksiPKS">Fraksi PKS</Select.Option>
                    <Select.Option value="FraksiPPP">Fraksi PPP</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="partai"
                    label="Partai"
                    rules={[
                        {
                            required: true,
                            message: "Please Select your partai!",
                        },
                    ]}
                >
                    <Select>
                    <Select.Option value="Select">Select</Select.Option>
                    <Select.Option value="PartaiGolongankarya">Partai Golongan Karya</Select.Option>
                    <Select.Option value="PartaiBulanBintang">Partai Bulan Bintang</Select.Option>
                    <Select.Option value="PartaiDemokrasIndonesiaPerjuangan">Partai Demokrasi Indonesia Perjuangan</Select.Option>
                    <Select.Option value="PartaiGerindra">Partai Gerindra</Select.Option>
                    <Select.Option value="PartaiHanura">Partai Hanura</Select.Option>
                    <Select.Option value="PartaiKeadilanSosial">Partai Keaadilan Sosial</Select.Option>
                    <Select.Option value="PartaiKebangkitanBangsa">Partai Kebangkitan Bangsa</Select.Option>
                    <Select.Option value="PartaiNasionalDemokrat">Partai Nasional Demokrat</Select.Option>
                    <Select.Option value="PartaiPersatuanPembangunan">Partai Persatuan Pembangunan</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="komisi"
                    label="Komisi"
                    rules={[
                        {
                            required: true,
                            message: "Please Select your Komisi!",
                        },
                    ]}
                >
                    <Select>
                    <Select.Option value="Select">Select</Select.Option>
                    <Select.Option value="I">I</Select.Option>
                    <Select.Option value="II">II</Select.Option>
                    <Select.Option value="III">III</Select.Option>
                    <Select.Option value="Pimpinan">Pimpinan</Select.Option>
                    </Select>
                </Form.Item>
               
                <Form.Item
                    name="address"
                    label="Address"
                    rules={[
                        {
                            required: true,
                            message: "Please input your address!",
                        },
                    ]}
                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item
                    name="phoneNumber"
                    label="Phone Number"
                    rules={[
                        {
                            required: true,
                            valdiator: (rule, value) => typeof value === "number" ? Promise.resolve() : Promise.reject('Phone number is invalid'),
                        },
                    ]}
                >
                    <Input/>
                </Form.Item> 
                <Form.Item
                    name="role"
                    label="Role"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Select>
                    <Select.Option value="staff">Staff</Select.Option>
                    <Select.Option value="admin">Admin</Select.Option>
                    </Select>
                </Form.Item> 
                <Form.List name="positions">
                    {(fields, { add, remove }) => (
                        <>
                        {fields.map(field => (
              <Space key={field.key} align="baseline">
                <Form.Item
                  {...field}
                  label="Position"
                //   name={[field.name, 'position']}
                  fieldKey={[field.fieldKey, 'Position']}
                  rules={[{ required: true, message: 'Please input position' }]}
                >
                  <Select style={{width: '30vw'}}>
                    {positions.map((position) => 
                        <Select.Option key={position.positionId} value={position.positionId}>{position.position}</Select.Option>
                    )}
                    </Select>
                </Form.Item>
                

                <MinusCircleOutlined onClick={() => remove(field.name)} />
              </Space>
            ))}

            <Form.Item>
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                Add New position
              </Button>
            </Form.Item>
          </>
        )}
      </Form.List>
                <Form.Item>
                    <Button type="primary" onClick={onRegister}>
                        Register
                    </Button>
                </Form.Item>
            </Form>
    </div>
};

export default CreateUserContainer;
