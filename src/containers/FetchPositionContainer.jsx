import React, { useState, useEffect } from "react";
import FetchPositionTemplate from "../components/FetchPositionTemplate";
import { useStoreState, useStoreActions } from "easy-peasy";

import {
    Space,
    Breadcrumb,
    Divider,
    Typography,
    Button
} from "antd";

import { 
    FileOutlined,
} from "@ant-design/icons";
import CreatePositionContainer from "./CreatePositionContainer";

const {Title, Text} = Typography; 

const FetchPositionContainer = () => {
    const fetchAllPositions = useStoreActions(actions => actions.adminFetchPosition.fetchAllPositions);
    const positions = useStoreState(state => state.adminFetchPosition.data.positions);

    const [fetched, setFetched] = useState(false);

    const onAddPosition = () => {
        setContent(<CreatePositionContainer onSubmit={onBackToMain} ></CreatePositionContainer>)
    }

    if (!fetched) {
        fetchAllPositions();
        setFetched(true);
    }

    useEffect(() => {
        setContent(<FetchPositionTemplate positions={positions} onAddPosition={onAddPosition}></FetchPositionTemplate>);
    }, [positions]);

    const [content, setContent] = useState(<FetchPositionTemplate positions={positions} onAddPosition={onAddPosition}></FetchPositionTemplate>);

    const onBackToMain = () => {
        setContent(<FetchPositionTemplate positions={positions} onAddPosition={onAddPosition}></FetchPositionTemplate>);
    }

    return<div>
        <Breadcrumb style={{ margin: '3px 0' }}>
            <Breadcrumb.Item>
                <Button type="link" disabled>
                    <FileOutlined />Positions
                </Button>
            </Breadcrumb.Item>
        </Breadcrumb>
        <Space direction="vertical" size={0} style={{ width: "100%" }}>
            <Title level={1}>Position Management</Title>
            <Text style={{ fontStyle: "italic"}}>Manage all position information</Text>
        </Space>
        <Divider style={{ borderWidth: 0.5, borderColor: 'black' }}></Divider>
        {content}
    </div>
};

export default FetchPositionContainer;
