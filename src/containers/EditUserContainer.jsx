import React, { useState } from "react";
import { Modal } from 'antd';

import {
    Space,
    Typography,
    Button,
    Form,
    Input,
    Select,
    DatePicker,
    Upload,
} from "antd";

import { 
    TeamOutlined, MinusCircleOutlined, PlusOutlined, UploadOutlined
} from "@ant-design/icons";
import { useStoreActions, useStoreState } from "easy-peasy";

const {Title, Text} = Typography; 
const { RangePicker } = DatePicker;

const EditUserContainer = ({user, onSubmit}) =>{
    const updateUser = useStoreActions(actions => actions.adminCrudUser.updateUser);
    const uploadImage = useStoreActions(actions => actions.adminCrudUser.uploadImage);
    const getImageUrl = useStoreActions(actions => actions.adminCrudUser.getImageUrl);
    const fetchAllPositions = useStoreActions(actions => actions.adminFetchPosition.fetchAllPositions);
    const positions = useStoreState(actions => actions.adminFetchPosition.data.positions);
    const [modal, contextHolder] = Modal.useModal();
    const [form] = Form.useForm();

    const [fetched, setFetched] = useState(false);

    if (!fetched) {
        fetchAllPositions();
        setFetched(true);
    }

    const onCreate = () => {
        form
            .validateFields()
            .then(async (value) => {
                let data = {...value, status: "Belum Dilaksanakan"};
                data.birthdate = data.birthdate._d;
                data.year = [data.year[0]._d, data.year[1]._d];
                console.log(data);
                const url = await getImageUrl(data.photo);
                if (url.error) {
                    data.photo = undefined;
                }
                else {
                    data.photo = url.data;
                }
                const res = await updateUser({userData: data, userId: user.userId});
                if (res.error) {
                    modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>), onOk: onSubmit})
                }
                else {
                    modal.info({title: 'Success', content:(<p>Document submitted</p>), onOk: onSubmit});
                }
            })
            .catch((info) => {
                console.log(info);
                modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>), onOk: onSubmit})
            });
    }

    return <div>
         <Form
                form={form}
                layout="vertical"
                name="create_form"
                initialValues={{
                    modifier: "public",
                }}
            >
                <Form.Item
                    name="nik"
                    label="NIK"
                    rules={[
                        {
                            required: true,
                            message: "Please input the NIK!",
                        },
                    ]}
                >
                    <Input placeholder={user.nik} />
                </Form.Item>
                <Form.Item
                    name="password"
                    label="Password"
                    rules={[
                        {
                            required: true,
                            message: "Please input the password!",
                        },
                    ]}
                >
                    <Input.Password placeholder={user.password} />
                </Form.Item>
                <Form.Item
                    name="confirmPassword"
                    label="Confirm Password"
                    rules={[
                        {
                            required: true,
                            message: "Please input the password!",
                        },
                        ({ getFieldValue }) => ({
                            validator(rule, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject('The two passwords that you entered do not match!');
                            },
                        }),
                    ]}
                >
                    <Input.Password placeholder={user.password} />
                </Form.Item>
                <Form.Item
                    name="name"
                    label="Name"
                    rules={[
                        {
                            required: true,
                            message: "Please input the name!",
                        },
                    ]}
                >
                    <Input placeholder={user.name} />
                </Form.Item>
                <Form.Item
                    name="photo"
                    label="Photo"
                    rules={[
                        {
                            required: true,
                            message: "Please upload your photo!",
                        },
                    ]}
                    valuePropName="checked"
                    getValueFromEvent={e => {
                        console.log(e);
                        return e.fileList[0].name;
                    }}
                >
                    <Upload
                        customRequest={uploadImage}
                        listType='picture'
                        className='upload-list-inline'
                    >
                        <Button icon={<UploadOutlined />}>Click to Upload</Button>
                    </Upload>
                </Form.Item>
                <Form.Item
                    name="gender"
                    label="Gender"
                    rules={[
                        {
                            required: true,
                            message: "Please Select the Gender!",
                        },
                    ]}
                >
                    <Select placeholder={user.gender}>
                    <Select.Option value="Male">Male</Select.Option>
                    <Select.Option value="Female">Female</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="birthdate"
                    label="BirthDate"
                    rules={[
                        {
                            required: true,
                            message: "Please input your birthdate!",
                        },
                    ]}
                >
                    <DatePicker />
                </Form.Item>
                <Form.Item
                    name="education"
                    label="Education"
                    rules={[
                        {
                            required: true,
                            message: "Please input your latest education!",
                        },
                    ]}
                >
                    <Input placeholder={user.education} />
                    </Form.Item>
                    <Form.Item
                    name="year"
                    label="Active Year"
                    rules={[
                        {
                            required: true,
                            message: "Please input your active year!",
                        },
                    ]}
                >
                    <RangePicker picker ='year' />
                    </Form.Item>
                <Form.Item
                    name="fraction"
                    label="Fraction"
                    rules={[
                        {
                            required: true,
                            message: "Please Select your fraction!",
                        },
                    ]}
                >
                    <Select placeholder={user.fraction}>
                    <Select.Option value="Select">Select</Select.Option>
                    <Select.Option value="FraksiGolkar">Fraksi Golkar</Select.Option>
                    <Select.Option value="FraksiAmanatSanggam">Fraksi Amanat Sanggam</Select.Option>
                    <Select.Option value="FraksiGabunganPerubahan">Fraksi Gabungan Perubahan</Select.Option>
                    <Select.Option value="FraksiPKS">Fraksi PKS</Select.Option>
                    <Select.Option value="FraksiPPP">Fraksi PPP</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="partai"
                    label="Partai"
                    rules={[
                        {
                            required: true,
                            message: "Please Select your partai!",
                        },
                    ]}
                >
                    <Select placeholder={user.partai}>
                    <Select.Option value="Select">Select</Select.Option>
                    <Select.Option value="PartaiGolongankarya">Partai Golongan Karya</Select.Option>
                    <Select.Option value="PartaiBulanBintang">Partai Bulan Bintang</Select.Option>
                    <Select.Option value="PartaiDemokrasIndonesiaPerjuangan">Partai Demokrasi Indonesia Perjuangan</Select.Option>
                    <Select.Option value="PartaiGerindra">Partai Gerindra</Select.Option>
                    <Select.Option value="PartaiHanura">Partai Hanura</Select.Option>
                    <Select.Option value="PartaiKeadilanSosial">Partai Keaadilan Sosial</Select.Option>
                    <Select.Option value="PartaiKebangkitanBangsa">Partai Kebangkitan Bangsa</Select.Option>
                    <Select.Option value="PartaiNasionalDemokrat">Partai Nasional Demokrat</Select.Option>
                    <Select.Option value="PartaiPersatuanPembangunan">Partai Persatuan Pembangunan</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name="komisi"
                    label="Komisi"
                    rules={[
                        {
                            required: true,
                            message: "Please Select your Komisi!",
                        },
                    ]}
                >
                    <Select placeholder={user.komisi}>
                    <Select.Option value="Select">Select</Select.Option>
                    <Select.Option value="I">I</Select.Option>
                    <Select.Option value="II">II</Select.Option>
                    <Select.Option value="III">III</Select.Option>
                    <Select.Option value="Pimpinan">Pimpinan</Select.Option>
                    </Select>
                </Form.Item>
               
                <Form.Item
                    name="address"
                    label="Address"
                    rules={[
                        {
                            required: true,
                            message: "Please input your address!",
                        },
                    ]}
                >
                    <Input.TextArea placeholder={user.address} />
                </Form.Item>
                <Form.Item
                    name="phoneNumber"
                    label="Phone Number"
                    rules={[
                        {
                            required: true,
                            valdiator: (rule, value) => typeof value === "number" ? Promise.resolve() : Promise.reject('Phone number is invalid'),
                        },
                    ]}
                >
                    <Input placeholder={user.phoneNumber} />
                </Form.Item> 
                <Form.List name="positions">
                    {(fields, { add, remove }) => (
                        <>
                        {fields.map(field => (
              <Space key={field.key} align="baseline">
                <Form.Item
                  {...field}
                  label="Position"
                //   name={[field.name, 'position']}
                  fieldKey={[field.fieldKey, 'Position']}
                  rules={[{ required: true, message: 'Please input position' }]}
                >
                  <Select style={{width: '30vw'}}>
                    {positions.map((position) => 
                        <Select.Option key={position.positionId} value={position.positionId}>{position.position}</Select.Option>
                    )}
                    </Select>
                </Form.Item>
                

                <MinusCircleOutlined onClick={() => remove(field.name)} />
              </Space>
            ))}

            <Form.Item>
              <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                Add New position
              </Button>
            </Form.Item>
          </>
        )}
      </Form.List>
      <Form.Item>
                    <Button type="primary" onClick={onCreate}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        {contextHolder}
    </div>
}

export default EditUserContainer;