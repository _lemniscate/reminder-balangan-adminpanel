import React, { useState, useEffect } from "react";
import FetchScheduleTemplate from "../components/FetchScheduleTemplate";
import FetchScheduleDetailTemplate from "../components/FetchScheduleDetailTemplate";
import EditScheduleContainer from "./EditScheduleContainer";
import { useStoreState, useStoreActions } from "easy-peasy";

import {
    Space,
    Breadcrumb,
    Divider,
    Typography,
    Button
} from "antd";

import { 
    FileOutlined,
} from "@ant-design/icons";
import CreateAddMemberContainer from "./CreateAddMemberContainer";

const {Title, Text} = Typography; 

const FetchScheduleContainer = () => {
    const fetchAllSchedules = useStoreActions(actions => actions.adminFetchSchedule.fetchAllSchedules);
    const fetchUsers = useStoreActions(actions => actions.adminFetchSchedule.fetchUsers);
    const schedules = useStoreState(state => state.adminFetchSchedule.data.schedules);
    const users = useStoreState(state => state.adminFetchSchedule.data.users);


    const [fetched, setFetched] = useState(false);

    if (!fetched) {
        fetchAllSchedules();
        setFetched(true);
    }

    const onScheduleEdit = (schedule) => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item onClick={onBackToAllScheduleMenu}>
                    <Button type="link">
                        <FileOutlined />Schedules
                    </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{schedule.title}</Breadcrumb.Item>
            </Breadcrumb>);
        setContent(<EditScheduleContainer scheduleId={schedule.scheduleId} onSubmit={onBackToAllScheduleMenu} />);
    }

    useEffect(() => {
        setContent(<FetchScheduleTemplate schedules={schedules} onScheduleClick={onScheduleClick} onEdit={onScheduleEdit} ></FetchScheduleTemplate>);
    }, [schedules]);

    const [breadcrumb, setBreadcrumb] = useState(<Breadcrumb style={{ margin: '3px 0' }}>
            <Breadcrumb.Item>
                <Button type="link" disabled>
                    <FileOutlined />Schedules
                </Button>
            </Breadcrumb.Item>
        </Breadcrumb>);

    const onAddUser = async (schedule) => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item onClick={onBackToAllScheduleMenu}>
                    <Button type="link">
                        <FileOutlined />Schedules
                    </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{schedule.title}</Breadcrumb.Item>
            </Breadcrumb>);
        await fetchUsers(schedule.scheduleId);
        setContent(<CreateAddMemberContainer scheduleId={schedule.scheduleId} />);
    }

    const onScheduleClick = async (schedule) => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item onClick={onBackToAllScheduleMenu}>
                    <Button type="link">
                        <FileOutlined />Schedules
                    </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{schedule.title}</Breadcrumb.Item>
            </Breadcrumb>);
        await fetchUsers(schedule.scheduleId);
        setContent(<FetchScheduleDetailTemplate schedule={schedule} users={users} onAddUser={onAddUser}></FetchScheduleDetailTemplate>);
    }

    const [content, setContent] = useState(<FetchScheduleTemplate schedules={schedules} onScheduleClick={onScheduleClick} onEdit={onScheduleEdit}></FetchScheduleTemplate>);

    const onBackToAllScheduleMenu = () => {
        setBreadcrumb(<Breadcrumb style={{ margin: '3px 0' }}>
                <Breadcrumb.Item>
                    <Button type="link" disabled>
                        <FileOutlined />Schedules
                    </Button>
                </Breadcrumb.Item>
            </Breadcrumb>);
        fetchAllSchedules();
        setContent(<FetchScheduleTemplate schedules={schedules} onScheduleClick={onScheduleClick} onEdit={onScheduleEdit}></FetchScheduleTemplate>);
    };

    return<div>
        {breadcrumb}
        <Space direction="vertical" size={0} style={{ width: "100%" }}>
            <Title level={1}>Schedule Management</Title>
            <Text style={{ fontStyle: "italic"}}>Manage all schedule information</Text>
        </Space>
        <Divider style={{ borderWidth: 0.5, borderColor: 'black' }}></Divider>
        {content}
    </div>
};

export default FetchScheduleContainer;
