import React from "react";
import { useStoreActions } from "easy-peasy";

import {
    Button,
    Form,
    Input,
    Modal
} from "antd";  

const CreatePositionContainer = ({onSubmit}) => {
    const submitPosition = useStoreActions(actions => actions.adminCreatePosition.submitPosition);
    const [modal, contextHolder] = Modal.useModal();
    const [form] = Form.useForm();

    const onCreate = () => {
        form
            .validateFields()
            .then(async (value) => {
                let data = {...value, status: false};
                console.log(data);
                const res = await submitPosition(data);
                if (res.error) {
                    modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>)})
                }
                else {
                    modal.info({title: 'Success', content:(<p>Document submitted</p>)});
                }
                onSubmit();
            })
            .catch((info) => {
                console.log(info);
                modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>)})
            });
    }

    return <div>
        <Form
                form={form}
                layout="vertical"
                name="create_form"
                initialValues={{
                    modifier: "public",
                }}
            >
                <Form.Item
                    name="position"
                    label="Title"
                    rules={[
                        {
                            required: true,
                            message: "Please input the title!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" onClick={onCreate}>
                        Create
                    </Button>
                </Form.Item>
            </Form>
            {contextHolder}
    </div>
};

export default CreatePositionContainer;
