import React from "react";
import {
    Form,
    Input,
    DatePicker,
    TimePicker,
    Breadcrumb,
    Button,
    Space,
    Typography,
    Divider
} from "antd";
import { Modal } from 'antd';
import { useStoreActions } from "easy-peasy";

import { 
    FileOutlined,
} from "@ant-design/icons";
  
const {Title, Text} = Typography; 
const { RangePicker } = TimePicker;

const EditScheduleContainer = ({scheduleId, onSubmit})=> {
    const editSchedule = useStoreActions(actions => actions.adminCrudSchedule.updateSchedule);
    const [modal, contextHolder] = Modal.useModal();
    const [form] = Form.useForm();

    const onCreate = () => {
        form
            .validateFields()
            .then(async (value) => {
                let data = {...value, status: "Belum Dilaksanakan"};
                data.start = data.start._d;
                console.log(data);
                const res = await editSchedule({scheduleData: data, scheduleId: scheduleId});
                if (res.error) {
                    modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>), onOk: onSubmit})
                }
                else {
                    modal.info({title: 'Success', content:(<p>Document submitted</p>), onOk: onSubmit});
                }
            })
            .catch((info) => {
                console.log(info);
                modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>), onOk: onSubmit})
            });
    }

    return <div>
            <Form
                form={form}
                layout="vertical"
                name="create_form"
                initialValues={{
                    modifier: "public",
                }}
            >
                <Form.Item
                    name="title"
                    label="Title"
                    rules={[
                        {
                            required: true,
                            message: "Please input the title!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item 
                    name="start" 
                    label="Date" 
                    rules={[
                        {
                            type: 'object',
                            required: true,
                            message: "Please input the date!",
                        },
                    ]}
                >
                    <DatePicker showTime format="YYYY-MM-DD " />
                </Form.Item>
                <Form.Item 
                    name="time" 
                    label="Duration" 
                    rules={[
                        {
                            type: 'object',
                            required: true,
                            message: "Please input the schedule duration!",
                        },
                    ]}
                    valuePropName="checked"
                    getValueFromEvent={e => {
                        console.log(e);
                        return {start: e[0]._d, end: e[1]._d};
                    }}
                >
                    <RangePicker  />
                </Form.Item>
                <Form.Item
                    name="location"
                    label="Location"
                    rules={[
                        {
                            required: true,
                            message: "Please input the location!",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="desc"
                    label="Description"
                    rules={[
                        {
                            required: true,
                            message: "Please input the description!",
                        },
                    ]}
                >
                    <Input.TextArea />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" onClick={onCreate}>
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        {contextHolder}
    </div>
}

export default EditScheduleContainer;