import React from "react";
import DashboardTemplate from "../components/DashboardTemplate";
// import { useStoreActions } from "easy-peasy";
import { useStoreState } from "easy-peasy";
import { Redirect } from "react-router-dom";

const DashboardContainer = ({ from, location }) => {
  const isAuthenticated = useStoreState((state) => state.auth.isAuthenticated);
  const user = useStoreState(state => state.auth.userData);

    console.log(user);
    if (!isAuthenticated)
    return (
      <Redirect
        to={{
          pathname: from ? from.pathname : "/login",
          state: { from: location },
        }}
      />
    );
  if ((user && user.role !== 'admin'))
        return <></>;
  else
    return (
      <DashboardTemplate></DashboardTemplate>
    );
};

export default DashboardContainer;
