import React from "react";
import { useStoreActions, useStoreState } from "easy-peasy";

import {
    Space,
    Breadcrumb,
    Divider,
    Typography,
    Button,
    Form,
    Input,
    DatePicker,
    Select,
    Modal
} from "antd";

import { 
    FileOutlined, PlusOutlined, MinusCircleOutlined
} from "@ant-design/icons";
  

const {Title, Text} = Typography; 

const CreateAddMemberContainer = ({scheduleId}) => {
    const addUser = useStoreActions(actions => actions.adminCrudSchedule.addUser);
    const users = useStoreState(state => state.adminFetchUser.data.users);
    const [modal, contextHolder] = Modal.useModal();
    const [form] = Form.useForm();

    const onCreate = () => {
        form
            .validateFields()
            .then(async (value) => {
                let data = {...value.userIds};
                data = Object.values(data);
                console.log(data);
                const res = await addUser({scheduleId, userIds:data});
                if (res.error) {
                    modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>)})
                }
                else {
                    modal.info({title: 'Success', content:(<p>Document submitted</p>)});
                }
            })
            .catch((info) => {
                console.log(info);
                modal.error({title: 'Error', content: (<p>Document cannot be submitted</p>)})
            });
    }

    return <div>
        {/* <Breadcrumb style={{ margin: '3px 0' }}>
            <Breadcrumb.Item>
                <Button type="link" disabled>
                    <FileOutlined /> 
                </Button>
            </Breadcrumb.Item>
        </Breadcrumb> */}
        {/* <Space direction="vertical" size={0} style={{ width: "100%" }}>
            <Title level={1}>Schedule Form</Title>
            <Text style={{ fontStyle: "italic"}}>Create new schedule</Text>
        </Space>
        <Divider style={{ borderWidth: 0.5, borderColor: 'black' }}></Divider> */}
        <Form
            form={form}
            layout="vertical"
            name="create_form"
            initialValues={{
                modifier: "public",
            }}
        >
            <Form.List name="userIds">
            {(fields, { add, remove }) => (
          <>
            {fields.map(field => (
              <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                <Form.Item
                  {...field}
                  name={[field.name, 'userId']}
                  fieldKey={[field.fieldKey, 'userId']}
                  rules={[{ required: true, message: 'Missing userId name' }]}
                >
                    <Select placeholder="User">
                        {users.map(user => <Select.Option key={user.userId} value={user.userId}>{user.name}</Select.Option>)}
                    </Select>
                </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                    </Space>
                ))}
                <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                        Add field
                    </Button>
                </Form.Item>
                </>
            )}
            </Form.List>
            <Form.Item>
                <Button type="primary" onClick={onCreate}>
                    Create
                </Button>
            </Form.Item>
        </Form>
        {contextHolder}
    </div>
};

export default CreateAddMemberContainer;
