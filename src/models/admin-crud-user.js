import { thunk } from "easy-peasy";

const adminCrudUser = {

    uploadImage: thunk(async (actions, payload, helpers) => {

        let { adminCrudUser } = helpers.injections;
        let res = await adminCrudUser.uploadImage(payload);
        if(res.error) return res;
        res = await actions.getImageUrl(res.data.metadata.name);
        console.log(res);
        return res;
    }),


    getImageUrl: thunk(async (actions, payload, helpers) => {

        let { adminCrudUser } = helpers.injections;
        let res = await adminCrudUser.getImageUrl(payload);
        return res;
    }),

    submitUser: thunk(async (actions, payload, helpers) => {

        let { adminCrudUser } = helpers.injections;
        let res = await adminCrudUser.submitUser(payload);
        return res;
    }),

    updateUser: thunk(async (actions, payload, helpers) => {

        let { adminCrudUser } = helpers.injections;
        let res = await adminCrudUser.updateUser(payload.userData, payload.userId);
        return res;
    }),

    deleteUser: thunk(async (actions, payload, helpers) => {

        let { adminCrudUser } = helpers.injections;
        let res = await adminCrudUser.deleteUser(payload);
        return res;
    })

}

export default adminCrudUser;