import { thunk } from "easy-peasy";

const adminCrudSchedule = {

    submitSchedule: thunk(async (actions, payload, helpers) => {

        let { adminCrudSchedule } = helpers.injections;
        let res = await adminCrudSchedule.submitSchedule(payload);
        return res;
    }),

    addUser: thunk(async (actions, payload, helpers) => {

        let { adminCrudSchedule } = helpers.injections;
        let res = await adminCrudSchedule.addUser(payload.scheduleId, payload.userIds);
        return res;
    }),

    updateSchedule: thunk(async (actions, payload, helpers) => {

        let { adminCrudSchedule } = helpers.injections;
        let res = await adminCrudSchedule.updateSchedule(payload.scheduleData, payload.scheduleId);
        return res;
    }),

    deleteSchedule: thunk(async (actions, payload, helpers) => {

        let { adminCrudSchedule } = helpers.injections;
        let res = await adminCrudSchedule.deleteSchedule(payload);
        return res;
    })

}

export default adminCrudSchedule;