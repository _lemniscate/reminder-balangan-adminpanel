import { persist, thunk, action } from "easy-peasy";

const adminFetchPosition = {
    fetchAllPositions: thunk(async (actions, payload, helpers) => {

        let { adminFetchPosition } = helpers.injections;
        let res = await adminFetchPosition.fetchAllPositions();
        actions.setPositions(res.data);
        return res;
    }),

    setPositions: action((state, payload) => {
        state.data.positions = payload;
    }),

    data: persist({
        positions: [],
    }),
}

export default adminFetchPosition;