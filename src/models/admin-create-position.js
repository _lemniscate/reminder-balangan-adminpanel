import { thunk } from "easy-peasy";

const adminCreatePosition = {

    submitPosition: thunk(async (actions, payload, helpers) => {

        let { adminCreatePosition } = helpers.injections;
        let res = await adminCreatePosition.submitPosition(payload);
        return res;
    }),

    deletePosition: thunk(async (actions, payload, helpers) => {

        let { adminCreatePosition } = helpers.injections;
        let res = await adminCreatePosition.deletePosition(payload);
        return res;
    })

}

export default adminCreatePosition;