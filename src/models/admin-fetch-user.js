import { thunk, action, persist, debug } from "easy-peasy";

const adminFetchUser = {
    fetchAllUsers: thunk(async (actions, payload, helpers) => {

        let { adminFetchUser } = helpers.injections;
        let res = await adminFetchUser.fetchAllUsers();
        actions.setUsers(res.data);
        return res;
    }),

    fetchUserAllAttendances: thunk(async (actions, payload, helpers) => {

        let { adminFetchUser } = helpers.injections;
        let res = await adminFetchUser.fetchUserAllAttendances(payload);
        console.log(res.data);
        actions.setAttendance(res.data);
        return res;
    }),

    fetchUserDailyAttendance: thunk(async (actions, payload, helpers) => {

        let { adminFetchUser } = helpers.injections;
        let res = await adminFetchUser.fetchUserDailyAttendance(payload);
        actions.setAttendance(res.data)
        return res;
    }),

    fetchUserWeeklyAttendance: thunk(async (actions, payload, helpers) => {

        let { adminFetchUser } = helpers.injections;
        let res = await adminFetchUser.fetchUserWeeklyAttendance(payload);
        actions.setAttendance(res.data)
        return res;
    }),

    fetchUserMonthlyAttendance: thunk(async (actions, payload, helpers) => {

        let { adminFetchUser } = helpers.injections;
        let res = await adminFetchUser.fetchUserMonthlyAttendance(payload);
        actions.setAttendance(res.data)
        return res;
    }),

    setUsers: action((state, payload) => {
        state.data.users = payload;
    }),

    setAttendance: action((state, payload) => {
        console.log(payload);
        state.data.attendances = payload;
        console.log(debug(state.data.attendances));
    }),

    data: persist({
        users: [],
        attendances: []
    }),

}

export default adminFetchUser;