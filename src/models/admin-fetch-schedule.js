import { thunk, action, persist } from "easy-peasy";

const adminFetchSchedule = {
    fetchAllSchedules: thunk(async (actions, payload, helpers) => {

        let { adminFetchSchedule } = helpers.injections;
        let res = await adminFetchSchedule.fetchAllSchedules();
        actions.setSchedule(res.data);
        return res;
    }),

    fetchUsers: thunk(async (actions, payload, helpers) => {

        let { adminFetchSchedule } = helpers.injections;
        let res = await adminFetchSchedule.fetchUsers(payload);
        actions.setUser(res.data);
        return res;
    }),

    setSchedule: action((state, payload) => {
        state.data.schedules = payload;
    }),

    setUser: action((state, payload) => {
        state.data.users = payload;
    }),

    data: persist({
        schedules: [],
        users: []
    }),

}

export default adminFetchSchedule;