import { thunk, action } from "easy-peasy";

const auth = {
    login: thunk(async (actions, payload, helpers) => {

        let { authService } = helpers.injections;
        let res = await authService.login(payload.email, payload.password);
        actions.setUserData(res.data);
        actions.setAuthenticated(true);
        return res;
    }),

    logout: thunk(async (actions, payload, helpers) => {

        let { authService } = helpers.injections;
        let res = await authService.logout();
        actions.setUserData(undefined);
        actions.setAuthenticated(false);
        return res;
    }),

    setUserData: action((state, payload) => {
        state.userData = payload;
    }),

    setAuthenticated: action((state, payload) => {
        state.isAuthenticated = payload;
    }),

    isAuthenticated: false,
    userData: undefined

}

export default auth;