import { thunk, action } from 'easy-peasy';

const master = {

    init: thunk(async (actions, payload, helpers) => {

        let { authService, adminFetchUser, adminCrudUser, adminFetchSchedule, adminCrudSchedule, adminCreatePosition, adminFetchPosition } = helpers.injections;    
        
        await authService.init();
        await adminFetchUser.init();
        await adminCrudUser.init();
        await adminFetchSchedule.init();
        await adminCrudSchedule.init();
        await adminCreatePosition.init();
        await adminFetchPosition.init();

        actions.setInitialized();
    }),

    setInitialized: action(state => {
        state.isInitialized = true;
    }),

    isInitialized: false

}

export default master;