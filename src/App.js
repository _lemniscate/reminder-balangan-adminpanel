import React, { useState } from 'react';
// import logo from './logo.svg';
import './App.css';

import { StoreProvider, createStore, useStoreState, useStoreActions } from "easy-peasy";
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";

import LoginContainer from "./containers/LoginContainer";
import DashboardContainer from "./containers/DashboardContainer";

import storeObj from "./integrations/store";

const store = createStore(storeObj.model, storeObj.option);

const ApplicationWrapper = ({ children }) => {
  const init = useStoreActions(actions => actions.master.init);
  const isInitialized = useStoreState(state => state.master.isInitialized);

  const [inited, setInit] = useState(false);
  if (!inited) {
    init();
    setInit(true);
  }

  if (isInitialized) return (<div>{children}</div>);
  else return (<div></div>);
};

function App() {
  return (
    <StoreProvider store={store}>
      <ApplicationWrapper>
        <Router>
          <Switch>
            {/* <PrivateRoute exact path="/dashboard"><DashboardContainer></DashboardContainer></PrivateRoute> */}
            <Route exact path="/login" component={LoginContainer}></Route>
            <Route exact path="/" component={DashboardContainer}></Route>
            <Route path="*">
              <Redirect to="/"></Redirect>
            </Route>
          </Switch>
        </Router>
      </ApplicationWrapper>
    </StoreProvider>
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
