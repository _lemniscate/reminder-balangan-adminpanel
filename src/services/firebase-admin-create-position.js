import * as firebase from "firebase/app";
import "firebase/firestore";
import FirebaseService from './firebase-service';

export default class FirebaseAdminCreatePosition extends FirebaseService {

    init = async () => {
        await this.baseInit();
        this.db = firebase.firestore();
    }

    submitPosition = async (data) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Position')
            .add(data)
            .then((snapshot) => {
                console.log("submit new document, ",snapshot.id);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    deletePosition = async (positionId) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Position')
            .doc(positionId)
            .delete()
            .then(() => {
                console.log("delete document, ",positionId);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }
}