import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import FirebaseService from './firebase-service';

export default class FirebaseAdminCrudUser extends FirebaseService {

    init = async () => {
        await this.baseInit();
        this.db = firebase.firestore();
        this.auth = firebase.auth();
        this.storage = firebase.storage();
    }

    uploadImage = async (file) => {

        console.log(file);
        let res = { data: undefined, error: undefined};
        await this.storage.ref(`imgs/${file.file.name}`)
            .put(file.file)
            .then(doc => {
                res.data = doc;
                console.log(doc);
            })
            .catch(e => {
                res.error = e;
            });
        return res;
    }

    getImageUrl = async (filename) => {

        let res = { data: undefined, error: undefined };
        await this.storage.ref(`imgs/`).child(filename)
            .getDownloadURL()
            .then(doc => {
                res.data = doc;
                console.log(doc)
            })
            .catch(e => {
                res.error = e;
            });
        return res;
    }

    submitUser = async (userData) => {
        let res = {data:[], error:undefined};

        let uid = undefined;
        await this.auth.createUserWithEmailAndPassword(userData.email, userData.password)
            .then(userRecord => uid = userRecord.user.uid)
            .catch(error => res.error = error);
        
        if (res.error) return res;

        let data = {...userData};
        delete data.email;
        delete data.password;

        console.log(data.birthdate._d);
        data.birthdate = data.birthdate._d;
        const year = [...data.year]
        data.year = [];
        year.forEach(y => data.year.push(y._d));
        console.log(data);

        await this.db.collection('Users').doc(uid)
            .set(data)
            .then((snapshot) => {
                console.log("submit new document, ",snapshot);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    updateUser = async (userData, userId) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Users').doc(userId)
            .update(userData)
            .then(() => {
                console.log("update document, ",userId);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    deleteUser = async (userId) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Users').doc(userId)
            .delete()
            .then(() => {
                console.log("delete document, ",userId);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }
}