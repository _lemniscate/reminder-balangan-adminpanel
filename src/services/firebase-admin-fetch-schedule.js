import * as firebase from "firebase/app";
import "firebase/firestore";
import FirebaseService from './firebase-service';

export default class FirebaseAdminFetchSchedule extends FirebaseService {

    init = async () => {
        await this.baseInit();
        this.db = firebase.firestore();
    }

    changeDate = (data) => {
        let newData = {...data, date: '', timeStamp: ''};
        newData.start = new Date(data.start.seconds*1000);
        newData.date = `${newData.start.getFullYear()}-${newData.start.getMonth()+1}-${newData.start.getDate()}`;
        const hour = newData.start.getHours() < 10 ? `0${newData.start.getHours()}` : newData.start.getHours();
        const minutes = newData.start.getMinutes() < 10 ? `0${newData.start.getMinutes()}` : newData.start.getMinutes();
        newData.timeStamp = `${hour}:${minutes}`;

        return newData;
    }

    fetchAllSchedules = async () => {

        let res = {data:[], error:undefined};

        await this.db.collection('Schedules').get()
            .then((snapshots) => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data.scheduleId = snapshot.id;
                    data = this.changeDate(data);
                    res.data.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    fetchUsers = async (scheduleId) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Schedules').doc(scheduleId).collection('members').get()
            .then((snapshots) => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data.userId = snapshot.id;
                    
                    res.data.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

}