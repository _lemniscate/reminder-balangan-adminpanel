import * as firebase from "firebase/app";
import "firebase/firestore";
import FirebaseService from './firebase-service';

export default class FirebaseAdminFetchPosition extends FirebaseService {

    init = async () => {
        await this.baseInit();
        this.db = firebase.firestore();
    }

    fetchAllPositions = async () => {

        let res = {data:[], error:undefined};

        await this.db.collection('Position')
            .orderBy('position')
            .get()
            .then((snapshots) => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data.positionId = snapshot.id;
                    res.data.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

}