import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import FirebaseService from './firebase-service';

export default class FirebaseAuthService extends FirebaseService {

    init = async () => {

        await this.baseInit();
        this.auth = firebase.auth();
        this.db = firebase.firestore();

    }

    login = async (email, password) => {

        let res = {data:undefined, error:undefined};

        await this.auth.signInWithEmailAndPassword(email, password)
            .then((userData) => {
                console.log(userData);
                res.data = userData;
            })
            .catch((error) => {
                res.error = error;
            });
        if (res.error) return res;
        
        await this.db.collection('Users').doc(res.data.user.uid)
            .get()
            .then(snapshot => {
                res.data = snapshot.data();
            }) 
            .catch(e => res.error = e);

        return res;
    }

    logout = async () => {

        let res = {data: undefined, error: undefined};
        await this.auth.signOut()
            .then(() => {
                res.data = "Ok";
            })
            .catch( error => {
                console.log(error);
            })

        return res;
    }

}