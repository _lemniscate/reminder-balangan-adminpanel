import * as firebase from "firebase/app";
import "firebase/firestore";
import FirebaseService from './firebase-service';

export default class FirebaseAdminFetchUser extends FirebaseService {

    init = async () => {
        await this.baseInit();
        this.db = firebase.firestore();
    }

    fetchAllUsers = async () => {

        let res = {data:[], error:undefined};

        await this.db.collection('Users')
            .orderBy('name')
            .get()
            .then((snapshots) => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data.userId = snapshot.id;
                    delete data.password;
                    res.data.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    changeDate = (data) => {
        let newData = {...data, date: '', timeStamp: ''};
        newData.create = new Date(data.create.seconds*1000);
        newData.date = `${newData.create.getFullYear()}-${newData.create.getMonth()+1}-${newData.create.getDate()}`;
        const hour = newData.create.getHours() < 10 ? `0${newData.create.getHours()}` : newData.create.getHours();
        const minutes = newData.create.getMinutes() < 10 ? `0${newData.create.getMinutes()}` : newData.create.getMinutes();
        newData.timeStamp = `${hour}:${minutes}`;

        return newData;
    }

    fetchUserAllAttendances = async (userId) => {

        let res = {data: [], error: undefined};

        await this.db.collection('Users').doc(userId).collection('attendance')
            .orderBy('create')
            .get()
            .then(snapshots => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data = this.changeDate(data);
                    data.attendanceId = snapshot.id;
                    res.data.push(data);
                })
            })
            .catch( error => {
                console.log(error);
            })

        return res;
    }

    fetchUserDailyAttendance = async (userId) => {
        let res = {data: [], error: undefined};

        const today = new Date();
        const yesterday = new Date(((today.getTime()/1000) - 86400)*1000);
        await this.db.collection('Users').doc(userId).collection('attendance')
            .orderBy('create')
            .startAt(yesterday)
            .endAt(today)
            .get()
            .then(snapshots => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data = this.changeDate(data);
                    data.attendanceId = snapshot.id;
                    res.data.push(data);
                })
            })
            .catch( error => {
                console.log(error);
            })

        return res;
    }

    fetchUserWeeklyAttendance = async (userId) => {
        let res = {data: [], error: undefined};

        const today = new Date();
        const lastWeek = new Date(((today.getTime()/1000) - 7*86400)*1000);
        await this.db.collection('Users').doc(userId).collection('attendance')
            .orderBy('create')
            .startAt(lastWeek)
            .endAt(today)
            .get()
            .then(snapshots => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data = this.changeDate(data);
                    data.attendanceId = snapshot.id;
                    res.data.push(data);
                })
            })
            .catch( error => {
                console.log(error);
            })

        return res;
    }

    fetchUserMonthlyAttendance = async (userId) => {
        let res = {data: [], error: undefined};

        const today = new Date();
        const lastMonth = new Date(((today.getTime()/1000) - 2678400)*1000);
        await this.db.collection('Users').doc(userId).collection('attendance')
            .orderBy('create')
            .startAt(lastMonth)
            .endAt(today)
            .get()
            .then(snapshots => {
                snapshots.forEach(snapshot => {
                    let data = snapshot.data();
                    data = this.changeDate(data);
                    data.attendanceId = snapshot.id;
                    res.data.push(data);
                })
            })
            .catch( error => {
                console.log(error);
            })

        return res;
    }

}