import * as firebase from "firebase/app";
import "firebase/firestore";
import FirebaseService from './firebase-service';

export default class FirebaseAdminCrudSchedule extends FirebaseService {

    init = async () => {
        await this.baseInit();
        this.db = firebase.firestore();
    }

    submitSchedule = async (scheduleData) => {

        let res = {data:[], error:undefined};

        await this.db.collection('Schedules')
            .add({...scheduleData, create: firebase.firestore.FieldValue.serverTimestamp()})
            .then((snapshot) => {
                console.log("submit new document, ",snapshot.id);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    addUser = async (scheduleId, userIds) => {
        let res = {data:[], error:undefined};

        console.log(scheduleId, userIds);
        for (let i = 0; i < userIds.length; i++) {
            await this.db.collection('Schedules').doc(scheduleId).collection('members').doc(userIds[i].userId)
                .set({status: "Belum Hadir"})
                .then((snapshots) => {
                    snapshots.forEach(snapshot => {
                        let data = snapshot.data();
                        data.userId = snapshot.id;
                        res.data.push(data);
                    });
                })
                .catch((error) => {
                    console.log(error);
                });
        }

        return res;
    }

    updateSchedule = async (scheduleData, scheduleId) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Schedules').doc(scheduleId)
            .update(scheduleData)
            .then(() => {
                console.log("update document, ",scheduleId);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

    deleteSchedule = async (scheduleId) => {
        let res = {data:[], error:undefined};

        await this.db.collection('Schedules').doc(scheduleId)
            .delete()
            .then(() => {
                console.log("delete document, ",scheduleId);
            })
            .catch((error) => {
                console.log(error);
            });

        return res;
    }

}