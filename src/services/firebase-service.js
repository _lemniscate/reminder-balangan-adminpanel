import * as firebase from "firebase/app";

export default class FirebaseService {

    baseInit = async () => {

        if (firebase.apps.length === 0)
        {
            const firebaseConfig = {
                apiKey: process.env.REACT_APP_FIREBASE_apiKey,
                authDomain: process.env.REACT_APP_FIREBASE_authDomain,
                databaseURL: process.env.REACT_APP_FIREBASE_databaseURL,
                projectId: process.env.REACT_APP_FIREBASE_projectId,
                storageBucket: process.env.REACT_APP_FIREBASE_storageBucket,
                messagingSenderId: process.env.REACT_APP_FIREBASE_messagingSenderId,
                appId: process.env.REACT_APP_FIREBASE_appId,
                measurementId: process.env.REACT_APP_FIREBASE_measurementId
            };
    
            firebase.initializeApp(firebaseConfig);
        }
    }
}