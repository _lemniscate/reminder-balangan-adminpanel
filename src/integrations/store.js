import master from "../models/master";
import auth from "../models/auth";
import adminFetchUser  from "../models/admin-fetch-user";
import adminCrudUser from "../models/admin-crud-user";
import adminFetchSchedule from "../models/admin-fetch-schedule";
import adminCrudSchedule from "../models/admin-crud-schedule";
import adminCreatePosition from "../models/admin-create-position";
import adminFetchPosition from "../models/admin-fetch-position";

import FirebaseAuthService from "../services/firebase-auth-service";
import FirebaseAdminFetchUser from "../services/firebase-admin-fetch-user";
import FirebaseAdminCrudUser from "../services/firebase-admin-crud-user";
import FirebaseAdminFetchSchedule from "../services/firebase-admin-fetch-schedule";
import FirebaseAdminCrudSchedule from "../services/firebase-admin-crud-schedule";
import FirebaseAdminCreatePosition from "../services/firebase-admin-create-position";
import FirebaseAdminFetchPosition from "../services/firebase-admin-fetch-position";

const firebaseAuthService = new FirebaseAuthService();
const firebaseAdminFetchUser = new FirebaseAdminFetchUser();
const firebaseAdminCrudUser = new FirebaseAdminCrudUser();
const firebaseAdminFetchSchedule = new FirebaseAdminFetchSchedule();
const firebaseAdminCrudSchedule = new FirebaseAdminCrudSchedule();
const firebaseAdminCreatePosition = new FirebaseAdminCreatePosition();
const firebaseAdminFetchPosition = new FirebaseAdminFetchPosition();

const store = {
    model: {
        master,
        auth,
        adminFetchUser,
        adminCrudUser,
        adminFetchSchedule,
        adminCrudSchedule,
        adminCreatePosition,
        adminFetchPosition,
      },
    option: {
        injections: {
            authService: firebaseAuthService,
            adminFetchUser: firebaseAdminFetchUser,
            adminCrudUser: firebaseAdminCrudUser,
            adminFetchSchedule: firebaseAdminFetchSchedule,
            adminCrudSchedule: firebaseAdminCrudSchedule,
            adminCreatePosition: firebaseAdminCreatePosition,
            adminFetchPosition: firebaseAdminFetchPosition,
        }
    }
}

export default store;