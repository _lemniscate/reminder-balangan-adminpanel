import React, { useState } from "react";
import {  Layout, Menu  } from "antd";
import { 
  FileOutlined,
  TeamOutlined,
  UserSwitchOutlined,
 } from "@ant-design/icons";

import FetchUserContainer from "../containers/FetchUserContainer";
import FetchScheduleContainer from "../containers/FetchScheduleContainer";
import FetchPositionContainer from "../containers/FetchPositionContainer";
import CreateUserContainer from "../containers/CreateUserContainer";
import CreateScheduleContainer from "../containers/CreateScheduleContainer";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const DashboardTemplate = () => {

    const [mainContainer, setMainContainer] = useState(<FetchScheduleContainer></FetchScheduleContainer>);

    const [collapsed, setCollapsed] = useState(false);
    const onCollapse = (collapse) => {
        setCollapsed(collapse);
    }

    const onMenuClick = (menu) => {
        if (menu === "userList") {
          setMainContainer(<FetchUserContainer></FetchUserContainer>);
        }
        else if (menu === "scheduleList") {
          setMainContainer(<FetchScheduleContainer></FetchScheduleContainer>);
        }
        else if (menu === "createUser") {
          setMainContainer(<CreateUserContainer></CreateUserContainer>);
        }
        else if (menu === "createSchedule") {
          setMainContainer(<CreateScheduleContainer></CreateScheduleContainer>)
        }
        else if (menu === "position") {
          setMainContainer(<FetchPositionContainer />);
        }
    }

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider
      style={{
        overflow: 'auto',
        minHeight: '100vh',
        position: 'fixed',
        left: 0,
      }}
      collapsible collapsed={collapsed} 
      onCollapse={onCollapse}
    >
      <div className="logo" />
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['scheduleList']}>
            <Menu.Item disabled key="empty"/>
            <SubMenu key="schedule" icon={<FileOutlined />} title="Schedule">
              <Menu.Item key="scheduleList" onClick={() => {onMenuClick("scheduleList")}}>Schedule List</Menu.Item>
              <Menu.Item key="createSchedule" onClick={() => {onMenuClick("createSchedule")}}>Create New Schedule</Menu.Item>
            </SubMenu>
            <SubMenu key="user" icon={<TeamOutlined />} title="Users">
              <Menu.Item key="userList" onClick={() => {onMenuClick("userList")}}>User List</Menu.Item>
              <Menu.Item key="createUser" onClick={() => {onMenuClick("createUser")}}>Create New User</Menu.Item>
            </SubMenu>
            <Menu.Item key="position" icon={<UserSwitchOutlined />} onClick={() => {onMenuClick("position")}}>Position</Menu.Item>
      </Menu>
    </Sider>
    <Layout>
    <Header className="site-layout-background" style={{ padding: 0 }} />
    <Layout className="site-layout" style={{ marginLeft: 200 }}>  
      <Content style={{ margin: '5% 5% 0', overflow: 'initial' }}>
            {mainContainer}
      </Content>
      <Footer style={{ textAlign: 'center' }}>DPRD Balangan ©2020</Footer>
      </Layout>
    </Layout>
  </Layout>

    // <Layout style={{ minHeight: '100vh' }}>
    //     <Layout className="site-layout">
    //       <Header className="site-layout-background" style={{ padding: 0 }} />
    //       <Layout>
    //       <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
    //       <div className="logo" />
    //       <Menu theme="dark" defaultSelectedKeys={['schdule']} mode="inline">
            // <SubMenu key="schedule" icon={<FileOutlined />} title="Schedule">
            //   <Menu.Item key="scheduleList">Schedule List</Menu.Item>
            //   <Menu.Item key="createSchedule">Create New Schedule</Menu.Item>
            // </SubMenu>
            // <SubMenu key="user" icon={<TeamOutlined />} title="Users">
            //   <Menu.Item key="userList" onClick={() => {onMenuClick("createNewUser")}}>User List</Menu.Item>
            //   <Menu.Item key="createUser">Create New User</Menu.Item>
            // </SubMenu>
    //       </Menu>
    //       </Sider>
    //       <Content style={{ margin: '0 16px' }}>
            // <Breadcrumb style={{ margin: '16px 0' }}>
            //   <Breadcrumb.Item>User</Breadcrumb.Item>
            //   <Breadcrumb.Item>Bill</Breadcrumb.Item>
            // </Breadcrumb>
            // {mainContainer}
    //       </Content>
    //       </Layout>
    //       <Footer style={{ textAlign: 'center' }}>Memang Mantap ©2020</Footer>
    //     </Layout>
    //   </Layout>
  );
};

export default DashboardTemplate;
