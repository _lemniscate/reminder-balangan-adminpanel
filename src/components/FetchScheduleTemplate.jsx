import React from "react";

import {
    List,
    Space,
    Button
} from "antd";
import { useStoreActions } from "easy-peasy";

const FetchScheduleTemplate = ({schedules, onScheduleClick, onEdit}) => {
    const deleteSchedule = useStoreActions(actions => actions.adminCrudSchedule.deleteSchedule);

    return(
    <>
    <Space direction="vertical" style={{ width: "100%" }}>
    <List
        style={{ borderWidth: 1, borderColor: '#969696' }}
        size="large"
        dataSource={schedules}
        renderItem={(schedule) =>
            <List.Item style={{ borderWidth: 1, borderColor: '#969696' }}>
                <List.Item.Meta 
                    title={schedule.title}
                    description={`Start: ${schedule.date}, ${schedule.timeStamp}`}
                />
                <Space direction="horizontal">
                    <Button type="primary" onClick={() => onScheduleClick(schedule)}>Detail</Button>
                    <Button type="primary" onClick={() => onEdit(schedule)}>Edit</Button>
                    <Button type="primary" danger onClick={() => deleteSchedule(schedule.scheduleId)}>Delete</Button>
                </Space>
            </List.Item>}
    ></List>
    </Space> 
    </>);
}

export default FetchScheduleTemplate;