import React from "react";
import FetchUserTemplate from "./FetchUserTemplate";

import {
    Button,
    Space, Typography
} from "antd";

const {Text, Title} = Typography;

const FetchScheduleDetailTemplate = ({schedule, users, onAddUser}) => {

    return (<div>
        <Title level={3}>Schedule Detail</Title>
        <Space size={'middle'}>
        <Space direction='vertical'>
            <Text>Title</Text>
            <Text>Status</Text>
            <Text>Start</Text>
            <Text>Location</Text>
            <Text>Description</Text>
        </Space>
        <Space direction='vertical'>
            <Text>{' : '}</Text>
            <Text>{' : '}</Text>
            <Text>{' : '}</Text>
            <Text>{' : '}</Text>
            <Text>{' : '}</Text>
        </Space>
        <Space direction='vertical'>
            <Text>{schedule.title ? schedule.title : '-'}</Text>
            <Text>{schedule.status ? schedule.status : '-'}</Text>
            <Text>{schedule.date ? `${schedule.date}, ${schedule.timeStamp}` : '-'}</Text>
            <Text>{schedule.location ? schedule.location : '-'}</Text>
            <Text>{schedule.desc ? schedule.desc : '-'}</Text>
        </Space>
        </Space>
        <p />
        <Title level={3}>Members</Title>
        <Button type="primary" onClick={() => onAddUser(schedule)}>Add new Member</Button>
        <FetchUserTemplate users={users} onUserClick={() => {}} />
        </div>);
}

export default FetchScheduleDetailTemplate;