import React from "react";

import {
    Calendar, Badge, Space, Typography, Image
} from "antd";

const {Text, Title} = Typography;

const FetchUserDetailTemplate = ({user, attendances}) => {

    const getListData = (value) => {
        const date = `${value._d.getFullYear()}-${value._d.getMonth()+1}-${value._d.getDate()}`;
        let listData = attendances.filter(attendance => attendance.date === date);
        return listData
    }

    const dateCellRender = (value) => {
        const listData = getListData(value);
        return (
          <ul className="events">
            {listData.map(item => (
              <li key={item.attendanceId}>
                <Badge status={'success'} text={item.type} />
              </li>
            ))}
          </ul>
        );
    }

    return (<div>
        <Title level={3}>User Detail</Title>
        <Space size={'middle'}>
        <Image height={375} width={280} src="http://dprd-balangankab.go.id/assets/images/photo/AHSANI_FAUZAN,_S.E_2.png"></Image>
        <Space direction='vertical'>
            <Text>name</Text>
            <Text>NIK</Text>
            <Text>Position</Text>
        </Space>
        <Space direction='vertical'>
            <Text>{' : '}</Text>
            <Text>{' : '}</Text>
            <Text>{' : '}</Text>
        </Space>
        <Space direction='vertical'>
            <Text>{user.name}</Text>
            <Text>{user.nik}</Text>
            <Text>{user.position}</Text>
        </Space>
        </Space>
        <Space direction='vertical' size='middle'>
        <Title level={3}>Attendance</Title>
        <Calendar dateCellRender={dateCellRender}/>
        </Space>
        </div>);
}

export default FetchUserDetailTemplate;