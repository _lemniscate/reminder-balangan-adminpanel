import React from "react";

import {
    List,
    Space,
    Button
} from "antd";
import { useStoreActions } from "easy-peasy";

const FetchPositionTemplate = ({positions, onAddPosition}) => {
    const deletePosition = useStoreActions(actions => actions.adminCreatePosition.deletePosition);

    return <Space direction="vertical" style={{ width: "100%" }}>
    <Button type="primary" onClick={onAddPosition}>Add new Position</Button>
    <List
        style={{ borderWidth: 1, borderColor: '#969696' }}
        size="large"
        dataSource={positions}
        renderItem={(position) =>
            <List.Item style={{ borderWidth: 1, borderColor: '#969696' }}>
                <List.Item.Meta 
                    title={position.position}
                />
                <Space direction="horizontal">
                    <Button type="primary" >Edit</Button>
                    <Button type="primary" danger onClick={() => deletePosition(position.positionId)} >Delete</Button>
                </Space>
            </List.Item>}
    ></List>
    </Space>;
}

export default FetchPositionTemplate;