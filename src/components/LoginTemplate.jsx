import React from "react";
import { Typography, Form, Input, Button, Layout, Row, Col } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

const { Content, Header, Footer } = Layout;
const { Title } = Typography;

const LoginTemplate = ({ onSubmitLogin }) => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
        <Header></Header>
        <Content>
        <Row type="flex" justify="center" align="middle" style={{minHeight: '65vh'}}>
        <Col span={8}>
        <Title>Reminder</Title>
        <Form
          name="normal_login"
          initialValues={{ remember: true }}
          onFinish={onSubmitLogin}
        >
          <Form.Item
            name="email"
            rules={[{ required: true, message: "Please input your Email!" }]}
          >
            <Input
              prefix={<UserOutlined />}
              placeholder="Email"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your Password!" }]}
          >
            <Input
              prefix={<LockOutlined />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
            >
              Log in
            </Button>
          </Form.Item>
        </Form>
        </Col>
        </Row>
        </Content>
        <Footer>DPRD Balangan ©2020</Footer>
    </Layout>
  );
};

export default LoginTemplate;
