import React, { useEffect, useState } from "react";

import {
    List,
    Space,
    Button,
    Avatar
} from "antd";
import { useStoreActions } from "easy-peasy";

const FetchUserTemplate = ({users, onUserClick, onEdit}) => {

    const getImageUrl = useStoreActions(actions => actions.adminCrudUser.getImageUrl);
    const [newUsers, setNewUsers] = useState([]);

    // useEffect(() => {
    //     users.forEach(user => {
    //         let imgUser = {...user};
    //         if (user.photo)
    //             imgUser.photo = getImageUrl(user.photo);
    //         else
    //             imgUser.photo = "https://firebasestorage.googleapis.com/v0/b/reminder-balangan.appspot.com/o/imgs%2FFoto%20Bapak%20Bapak.png?alt=media&token=cf217830-752d-4788-a480-1fc95e751f3b"
    //         setNewUsers([newUsers, imgUser]);
    //     });
    // }, []);

    return <Space direction="vertical" style={{ width: "100%" }}>
    <List
        style={{ borderWidth: 1, borderColor: '#969696' }}
        size="large"
        dataSource={users}
        renderItem={(user) =>
            <List.Item style={{ borderWidth: 1, borderColor: '#969696' }}>
                <List.Item.Meta 
                    avatar={<Avatar shape='square' size='large' src="https://firebasestorage.googleapis.com/v0/b/reminder-balangan.appspot.com/o/imgs%2FFoto%20Bapak%20Bapak.png?alt=media&token=cf217830-752d-4788-a480-1fc95e751f3b" />}
                    title={user.name}
                    description={"NIK: "+user.nik}
                />
                <Space direction="horizontal">
                    <Button type="primary" onClick={() => onUserClick(user)}>Detail</Button>
                    <Button type="primary" onClick={() => {onEdit(user)}}>Edit</Button>
                    <Button type="primary" danger >Delete</Button>
                </Space>
            </List.Item>}
    ></List>
    </Space>
}

export default FetchUserTemplate;